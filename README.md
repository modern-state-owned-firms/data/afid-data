Research project

# 'Modern state-owned firms' performance: An empirical analysis of productivity, market power, and innovation'
financed by the German Science Foundation (DFG) | 2019-2022

Authors of the following codes: Julia Rechlitz (@jrechlitz), Caroline Stiel (@cjstiel), Nicole Wägner (@nwaegner)

All codes here provided are open source. All codes are licenced under the [MIT License](https://spdx.org/licenses/MIT.html) expanded by the [Commons Clause](https://commonsclause.com/). All data sets are licenced under the [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## MAIN DATA I: Amtliche Firmendaten in Deutschland (AFiD)

In this project, we merge the following different AFiD sub data sets: 

| No. |  data set | Link to variable list|
|-----|-----------|----------------------|
1|[AFiD-Panel Energieunternehmen (EVAS 43221)](https://doi.org/10.21242/43221.2017.00.01.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/afid-panel_energieunternehmen_md.pdf)|
2|[AFiD-Panel Energiebetriebe (EVAS 43212)](https://doi.org/10.21242/43212.2017.00.01.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/afid-panel_energiebetriebe_md.pdf)|
3|[Erhebung über Erzeugung, Bezug, Verwendung und Abgabe von Wärme (EVAS 43411)](https://doi.org/10.21242/43411.2016.00.00.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/waerme_mml.pdf)|
4|[Erhebung über Stromabsatz und Erlöse der Elektrizitätsversorgungsunternehmen und Stromhändler (EVAS 43331)](https://doi.org/10.21242/43331.2016.00.00.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/strom_erloese_mml.pdf)|
5|[Monatsbericht über die Elektrizitäts- und Wärmeerzeugung der Stromerzeugungsanlagen für die allgemeine Versorgung (EVAS 43311)](https://doi.org/10.21242/43311.2016.00.00.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/mb_stromerzeugung_mml.pdf)|
6|[Monatsbericht über die Elektrizitätsversorgung der Netzbetreiber (EVAS 43312)](https://doi.org//10.21242/43312.2016.00.00.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/mb_elektrizit%C3%A4t_netz_mml.pdf)|
7|[AFiD-Modul Umweltschutzinvestitionen (EVAS 32511)](https://doi.org/10.21242/32511.2017.00.03.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/umweltschutzinvestitionen_2006-2016_mdr_teil_ii_on-site_0.pdf)|
8|[AFiD-Erhebung über die öffentliche Wasserversorgung (EVAS 32211)](https://doi.org/10.21242/32211.2016.00.00.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/wasserversorgung_2016_on-site_dsb.pdf)|
9|[Jahresabschlüsse der kaufmännisch buchenden Extrahaushate und der kaufmännisch buchenden sonstigen öffentlichen Fonds, Einrichtungen und Unternehmen (EVAS 71327)](https://doi.org/10.21242/71811.2016.00.00.1.1.0)|[Link](https://www.forschungsdatenzentrum.de/sites/default/files/jahresabschluss_ab%201998_mml.pdf)|
   
   
## MAIN DATA II: External data on electricity distribution from ene't

|No.| data set | Link to the variable list |
|---|----------|---------------------------|
10|_Netznutzung Strom_ provided by [ene't](www.enet.eu)|[Link](https://download.enet.eu/download/ausliefern/quelle/198/nns-access-2021-07-01.pdf)|
    


## Data preparation: Merging the MAIN DATA

**Step 1: Eliminate duplicates**

---------------------------------------------------
 
Start with the R code (master file) ```01_delete_duplicates_in_evas_43312_and_43311.r```.

   It executes the codes ```01a_delete_duplicates_in_evas_43312.r``` which deletes duplicates in the data set _Monatsbericht über die Elektrizitätsversorgung der Netzbetreiber [6]_. Furthermore, it runs the code ```01b_delete_duplicates_in_evas_43311.r``` which deletes duplicates in the data set _Monatsbericht über die Elektrizitäts- und Wärmeerzeugung der Stromerzeugungsanlagen für die allgemeine Versorgung [5]_.
   
   Output are the data sets ```4193_kraftw_unique_2005-2016.dta``` and 
   ```4193_Netze_unique_2005-2016.dta```.
   

**Step 2: Merge the different AFID data sets [1] through [6]**

---------------------------------

The STATA do-file ```02_afid_energy_data.do``` (master file) executes the do-file ```02a_merge_afid_energy_data.do```, which merges most of the special AFiD surveys [1], [2], [3], [4], ```4193_kraftw_unique_2005-2016.dta``` [5] and ```4193_Netze_unique_2005-2016.dta``` [6]. In the end, the AFiD energy panel is obtained, which includes data on firm and plant level. Afterward the do-file ```02b_review_afid_energy_data.do``` reviews the newly merged data set and computes some descriptives. 
   
   Output is the data set ```energy0516.dta```.

   
   
**Step 3: Add the balance sheet data for public entities [9]**

------------------------------------------------------------------

Then, the AFiD energy panel is extended by the data set _Jahresabschlüsse der kaufmännisch buchenden Extrahaushalte und der kaufmännisch buchenden sonstigen öffentlichen Fonds, Einrichtungen und Unternehmen [9]_ (later called JAB). Use for this the R code (master file) ```03_merge_AFiD_energy_and_JAB.r```, which executes the R code ```03a_execute_merge_AFiD_energy_and_JAB.r```. Notice that only those observations from the JAB are kept, which were successfully merged with the AFiD energy panel. 
   
Output is the data set ```4193_AFiD+Jab_2005-2016.rds```. 

   
   
**Step 4: Add the data on investments into environmental protection [7]**

----------------------------------------------------------------------


 Afterward, the data sets on investments in environmental protection are processed. It is important to know that _AFiD-Modul Umweltschutzinvestitionen [7]_ consists of two data set, one is on firm level, and one is on plant level.  The master file ```04_merge_investments_in_environmental_protection.r``` executed the R code ```04a_execute_merge_investments_in_environmental_protection.r```. First, this code merges the two data sets of the _AFiD-Modul Umweltschutzinvestitionen [7]_ to a common one. Second, the data on investments in environmental protection is merged with the data set ```4193_AFiD+Jab_2005-2016.rds```. When merging the _AFiD-Modul Umweltschutzinvestitionen [7]_ on plant level with the AFiD energy panel we use as identifier ```unr```, ```bnr``` and ```year```. When only using ```bnr``` and ```year``` as identifiers (which leads to more merges), some plants are merged to firms they do not belong to.
   
   Outputs are the data sets ```4193_UIU+UIB_2005-2016.rds``` and
                             ```4193_AFiD+Jab+Umwelt_2005-2016.rds```.
   
   
**Step 5: Add the ene't data [10]**

-----------------------------------------------------
   
a) The data set _Netznutzung Strom [10]_, which is an external data set provided by the ene't company is merged with the AfiD data set using the master file ```05_merge_enet_afid.r``` which executes the code ```05a_executemerge_enet_afid.r```.
The codes preparing the ene't data for this merge can be found [here](https://gitlab.com/modern-state-owned-firms/enet-data).
   
   Output is the data set```"4193_AFiD+Jab+Umwelt+Enet_2005-2016.rds```.

**As a result, we obtain a final panel data set on plant level, which covers the years from 2005 to 2016.**

_Notes: The survey on water supply, AFiD-Erhebung über die öffentliche Wasserversorgung [8], is not part of the merged data set. The codes for preparing this dataset can be found [here](https://gitlab.com/modern-state-owned-firms/panel-of-german-water-suppliers)._
