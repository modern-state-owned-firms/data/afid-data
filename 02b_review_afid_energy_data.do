
* Initial preparation
*----------------------
set more off
clear all

local date=ltrim("$S_DATE")
local date=subinstr("`date'"," ","_",2)
local VERSION "v02"


* Start log file
*-----------------
cap log close
log using "$LOGDIR\LOG_02b_review_afid_energy_data_`date'.log", replace

********************************************************************************
*
*							    	Project 4193
*
* 						Review of the AFiD energy data
*
* -----------------------------------------------------------------------------*
* Authors: Caroline Stiel (cs), Julia Rechlitz (jr)
* Phone: 030-89789-514 (cs)
* Mail: cstiel@diw.de, jrechlitz@diw.de
*
* Name of the code: 02b_review_afid_energy_data.do
* Name of the log file: LOG_02b_review_afid_energy_data_<date>.log
* -----------------------------------------------------------------------------*

* -----------------------------------------------------------------------------*
* Used data set: energie0516.dta
* Path: R:\Projekt4193\Bearbeiterdaten\Programme\Aufbereitung Energiedaten\
*   Arbeitsdateien\energie0516.dta
* ------------------------------------------------------------------------------

********************************************************************************
* 									START
********************************************************************************

* Load data
*--------------
use "$ARBEITSDIR\energie0516.dta", clear


* Number of observations per year
* ---------------------------------
tab jahr, mi


* Create a data set on firm level
*-----------------------------------
duplicates drop unr jahr, force

save "$ARBEITSDIR\TEMP1.dta", replace


********************************************************************************
* 1) Proof the consistency of the information on the economic sector
*
*    The data set contains different variables which describe the 
*    economic sector the firm is mainly active in 
********************************************************************************

* --------------------------*
* 1.1 Firm level
* --------------------------*

* compare the variables stromein_wz, netz_wz, peu_wz
*----------------------------------------------------
use "$ARBEITSDIR\TEMP1.dta", clear
destring stromein_wz absatz_wz netz_wz peu_wz, replace

tab jahr, mi
tab jahr if (peu_wz !=. & netz_wz !=.) & peu_wz != netz_wz
tab jahr if (peu_wz !=. & stromein_wz !=.) & peu_wz != stromein_wz
tab jahr if (peu_wz !=. & absatz_wz !=.) & peu_wz != absatz_wz


* --------------------------*
* 1.2 Plant level
* --------------------------*

* Compare the variables waerme_wz, kraftw_wz, peb_wz 
*----------------------------------------------------
use "$ARBEITSDIR\energie0516.dta", clear
destring waerme_wz kraftw_wz peb_wz, replace

tab jahr, mi
tab jahr if (peb_wz !=. & waerme_wz !=.) & peb_wz != waerme_wz
tab jahr if (peb_wz !=. & kraftw_wz !=.) & peb_wz != kraftw_wz 


********************************************************************************
* 2) Proof the consistency of the information on state
*
*    The data set contains different variables which describe in which
*    German state the firm is located at
********************************************************************************

* --------------------------*
* 2.1 Firm level
* --------------------------*

* Compare the variables bl_u, peb_bl_u, netz_bl, peu_land
*---------------------------------------------------------
use "$ARBEITSDIR\TEMP1.dta", clear

tab jahr, mi
tab jahr if (peu_land !=. & peb_bl_u !=.) & peu_land != peb_bl_u
tab jahr if (peu_land !=. & netz_bl !=.) & peu_land != netz_bl
tab jahr if (peu_land !=. & stromein_land !=.) & peu_land != stromein_land
tab jahr if (peu_land !=. & absatz_land !=.) & peu_land != absatz_land


* --------------------------*
* 2.2 Plant level
* --------------------------*

* Compare the variables waerme_land, kraftw_land, peb_land
*----------------------------------------------------------
use "$ARBEITSDIR\energie0516.dta", clear

tab jahr, mi
tab jahr if (peb_land !=. & waerme_land !=.) & peb_land != waerme_land
tab jahr if (peb_land !=. & kraftw_land !=.) & peb_land != kraftw_land


********************************************************************************
* 3) Proof the consistency of the information on the AGS 
*    (Amtlicher Gemeindeschluessel)
*
*    The data set contains different variables which describe in which
*    municipality the firm is located at
********************************************************************************

* --------------------------*
* 3.1 Firm level
* --------------------------*

* Compare the variables ags_u, netz_ags, peu_ags
*------------------------------------------------
use "$ARBEITSDIR\TEMP1.dta", clear

tab jahr if (peu_ags !="" & netz_ags !="") & peu_ags!= netz_ags
tab jahr if (peu_ags !="" &  stromein_ags!="") & peu_ags != stromein_ags
tab jahr if (peu_ags !="" &  absatz_ags!="") & peu_ags != absatz_ags


* --------------------------*
* 3.2 Plant level
* --------------------------*

* Compare the variables waerme_ags, kraftw_ags, peb_ags
*-------------------------------------------------------
use "$ARBEITSDIR\energie0516.dta", clear

tab jahr if (peb_ags !="" & waerme_ags !="") & peb_ags != waerme_ags
tab jahr if (peb_ags !="" & kraftw_ags !="") & peb_ags != kraftw_ags


********************************************************************************
* 4) Analyse in which questionnairs the firms/plants participate
********************************************************************************

*------------------------------*
* 4.1 Firm level
*------------------------------*

use "$ARBEITSDIR\TEMP1.dta", clear

tab jahr, mi

* Number of firms participating on the KSE
* -------------------------------------------
tab jahr evuk, mi


* Number of firms participating on the IVE
* -------------------------------------------
tab jahr evu, mi


* Number of firms participating on the main AFiD-Panel for firms
* --------------------------------------------------------------
tab jahr peu, mi


* Number of firms participating on 066N (Netzbetreiber)
* ------------------------------------------------------------
tab jahr TM_066N, mi


*  Number of firms participating on 070 (Netzbetreiber)
* -----------------------------------------------------------
tab jahr TM_070, mi


*  Number of firms participating on 083 (Stromabsatz)
* ---------------------------------------------------------
tab jahr TM_083, mi


*------------------------------*
* 4.2 Plant level
*------------------------------*

use "$ARBEITSDIR\energie0516.dta", clear


* Number of plants participating on the main AFiD-Panel for plants
* --------------------------------------------------------------------
tab jahr peb, mi


* Number of plants participating on IVE for plants
* --------------------------------------------------
tab jahr evb, mi


* Number of plants participating on 065 (Monatsberichte)
* ---------------------------------------------------------
tab jahr evmb, mi


* Number of plants participating on 066K (Kraftwerke)
* ---------------------------------------------------------
tab jahr TM_066K, mi


* Number of plants participating on 064 (Waerme)
* ----------------------------------------------------
tab jahr TM_064, mi


*------------------------------------------------------------------------------------------------*
* 4.3 Check why some firms are part of some special surveys but are not part of the KSE?
*------------------------------------------------------------------------------------------------*
use "$ARBEITSDIR\TEMP1.dta", clear


* Number of firms per year, which do not participate in the KSE
*--------------------------------------------------------------
tab jahr if evuk == .


* From which surveys do those firms originate from?
*--------------------------------------------------
tab jahr evu if evuk == ., mi
tab jahr peu if evuk == ., mi
tab jahr TM_066N if evuk == ., mi
tab jahr TM_070 if evuk == ., mi
tab jahr TM_083 if evuk == ., mi


* In which state are those firms are located?
* --------------------------------------------
tab peu_land if evuk == ., mi


* Do we find KSE entries for those firms in later or earlier years?
* Compute a dummy indicating whether a firm has no KSE for a certain year. 
*-------------------------------------------------------------------------
gen ohneKSE = 0
replace ohneKSE=1 if evuk==.


* Calculate the arithmetic mean of the variable ohneKSE for each frim
* it shows you the share of years the firm did not participate in the KSE
*---------------------------------------------------------------------------
bysort unr: egen D_kse = mean(ohneKSE)
tab D_kse, mi


*---------------------------------------------------------------------------*
* 4.4 Number of Observations in special surveys with KSE match (firm level)
*---------------------------------------------------------------------------*

*------------------------------*
* 4.4.1 Firm level 
*------------------------------*

use "$ARBEITSDIR\TEMP1.dta", clear


* Number of plants participating on 066N (Netzbetreiber)
* ------------------------------------------------------------
tab jahr TM_066N if peu==1, mi


* Number of plants participating on 070 (Netzbetreiber)
* -----------------------------------------------------------
tab jahr TM_070 if peu==1, mi


* Number of plants participating on 083 (Stromabsatz)
* ---------------------------------------------------------
tab jahr TM_083 if peu==1, mi


*------------------------------*
* 4.4.2 Plant level
*------------------------------*

use "$ARBEITSDIR\energie0516.dta", clear

* Number of plants participating onthe main AFiD-Panel for plants
* ----------------------------------------------------------------
tab jahr peb if peu==1, mi


* Number of plants participating on IVE Betriebe
* --------------------------------------------------
tab jahr evb if peu==1, mi


* Number of plants participating on 065 (Monatsberichte)
* ---------------------------------------------------------
tab jahr evmb if peu==1, mi


* Number of plants participating on 066K (Kraftwerke)
* ---------------------------------------------------------
tab jahr TM_066K if peu==1, mi


* Number of plants participating on 064 (Waerme)
* ----------------------------------------------------
tab jahr TM_064 if peu==1, mi


********************************************************************************
* 5) clean and save of the final data set
********************************************************************************

cap noi erase "$ARBEITSDIR\TEMP1.dta"

********************************************************************************
* End of file
********************************************************************************
 
 
 cap log close
