*initial preparation
set more off
clear all
cap log close

********************************************************************************
*
*							      	Project 4193
*
* 	      Data Proceeding AFiD_Energiepanel- MASTERFILE
*
* -----------------------------------------------------------------------------*
* Authors: Caroline Stiel, Julia Rechlitz 
* Telefon: 030-89789-514 (cs)
* Mail: cstiel@diw.de, jrechlitz@diw.de
*
* Name of the code: 02_afid_energy_data.do
* Name of the log file: none
* -----------------------------------------------------------------------------*

********************************************************************************
* 1) Define the working environment
********************************************************************************

global GWAP 0 /* 0: KDFV 1: GWAP*/


if $GWAP==1{
	global DATADIR	"R:\Projekt4193\Originaldaten"
	global ARBEITSDIR "R:\Projekt4193\Bearbeiterdaten\Programme\Arbeitsdateien"
	global LOGDIR "R:\Projekt4193\Bearbeiterdaten\Programme\Merge Data Sets\Ergebnisse"
	global PROGRAMS "R:\Projekt4193\Bearbeiterdaten\Programme\Merge Data Sets"
}

if $GWAP==0{
	global DATADIR "Q:\AfS\55_FDZ\Forschungsprojekte\2020-4193 DIW Röhr (26)\Daten"
	global ARBEITSDIR "Q:\AfS\55_FDZ\Forschungsprojekte\2020-4193 DIW Röhr (26)\Daten\Arbeitsdateien"
	global LOGDIR "Q:\AfS\55_FDZ\Forschungsprojekte\2020-4193 DIW Röhr (26)\KDFV\2022_10_17_jr3" 
	global PROGRAMS $LOGDIR
	adopath +"Q:\AfS\55_FDZ\Tools\STATA\ado"
}

********************************************************************************
* 2)  Call different do files
********************************************************************************

* Aufbereitung des Energiepanels
do "$PROGRAMS\02a_merge_afid_energy_data.do"
do "$PROGRAMS\02b_review_afid_energy_data.do"

********************************************************************************
* End of file
********************************************************************************

