#==============================================================================
# Master file
#
# Merges the data set "Netznetzung strom" provided by ene't wth the AFiD 
# energy panel. The firm ID and the year are used as identifyer 
#
# Authors: Julia Rechlitz                                                                                    
#                                                                                    
# Calls the following R code:
# "03a_execute_merge_enet_afid" 
#==============================================================================

#==============================================================================
# 1. Set up R
#==============================================================================

# clean memory 
#--------------
rm(list = ls())

# Tell R to use fixed notation instead of exponential notation
#-------------------------------------------------------------
options(scipen = 999)


#==============================================================================
# 2. Define working environment
#==============================================================================

# The variable FDZ describes, where the code is executed
#	--- 0: own computer
#	--- 1: KDFV (original data set)
# ---	2: GWAP (pseudonymized original data, possibly without Bavaria)
#--------------------------------------------------------------------
FDZ <- -1


#	Working environment 0: own computer (DIW)
#	-----------------------------------------

if (FDZ == -1)  {
  datenpfad_enet  <- "Y:/projects/current/efficiency/14_DFG-Projekt/03_daten/02_Externe Daten/enet" 
  syntaxpfad      <- "Y:/projects/current/efficiency/14_DFG-Projekt/03_daten/03_merge_code/afid-data" 
  outputpfad      <- "Y:/projects/current/efficiency/14_DFG-Projekt/03_daten/00_FDZ_Datenstrukturfiles/Ergebnisse" 
  neudatenpfad    <- "Y:/projects/current/efficiency/14_DFG-Projekt/03_daten/00_FDZ_Datenstrukturfiles/Arbeitsdateien"
  
  dateiname_afid    <- "4193_AFiD+Jab_2005-2016.rds"
  dateiname_enet    <- "Enet_panel_v16_readyforFDZ.dta"
  outputname        <- "log_05_merge_enet_afid" 
  syntaxname        <- "05a_execute_merge_enet_afid"
  
  .libPaths("Y:/projects/current/efficiency/14_DFG-Projekt/03_daten/04_packages/01_packages_jr")
}  



#	Working environment 0: own computer
#	-----------------------------------

if (FDZ == 0)  {
  datenpfad         <- "C:/Users/julia/Documents/Promotion/Projekte/gitLab/01_original_data"
  syntaxpfad        <- "C:/Users/julia/Documents/Promotion/Projekte/gitLab/afid-data/afid-data" 
  outputpfad        <- "C:/Users/julia/Documents/Promotion/Projekte/gitLab/02_outputs/01_DSF" 
  neudatenpfad      <- "C:/Users/julia/Documents/Promotion/Projekte/gitLab/02_outputs/01_DSF" 
  
  dateiname_afid    <- "4193_AFiD+Jab+Umwelt_2005-2016.rds"
  dateiname_enet    <- "4193_enet_2005-2016.dta"
  outputname        <- "log_05_merge_enet_afid" 
  syntaxname        <- "05a_execute_merge_enet_afid"
  
}


#	Working environment 1: KDFV
#	----------------------------

if (FDZ == 1) {
  datenpfad         <- "Q:/AfS/55_FDZ/Forschungsprojekte/2020-4193 DIW Röhr (26)/Daten" 
  syntaxpfad        <- "Q:/AfS/55_FDZ/Forschungsprojekte/2020-4193 DIW Röhr (26)/KDFV/2022_10_17_jr6"
  outputpfad        <- "Q:/AfS/55_FDZ/Forschungsprojekte/2020-4193 DIW Röhr (26)/KDFV/2022_10_17_jr6"
  neudatenpfad      <- "Q:/AfS/55_FDZ/Forschungsprojekte/2020-4193 DIW Röhr (26)/Daten/Arbeitsdateien"
  
  dateiname_afid    <- "4193_AFiD+Jab+Umwelt_2005-2016.rds"
  dateiname_enet    <- "4193_enet_2005-2016.dta"
  outputname        <- "log_05_merge_enet_afid" 
  syntaxname        <- "05a_execute_merge_enet_afid"
  
 # .libPaths("Q:/AfS/55_FDZ/Forschungsprojekte/2020-4193 DIW Röhr (26)/Daten/Arbeitsdateien/Rpakete")
  .libPaths("Q:/AfS/55_FDZ/Forschungsprojekte/2020-4193 DIW Röhr (26)/Daten/Arbeitsdateien/RPakete_v4")
}


#	Working environment 2: GWAP
#	---------------------------

if (FDZ == 2) {
  datenpfad         <- "R:/Projekt4193/Originaldaten" 
  syntaxpfad        <- "R:/Projekt4193/Bearbeiterdaten/Programme/Merge Data Sets"
  outputpfad        <- "R:/Projekt4193/Bearbeiterdaten/Programme/Merge Data Sets/Ergebnisse" 
  neudatenpfad      <- "R:/Projekt4193/Bearbeiterdaten/Programme/Arbeitsdateien"
  
  dateiname_afid    <- "4193_AFiD+Jab+Umwelt_2005-2016.rds"
  dateiname_enet    <- "4193_enet_2005-2013_p_ohne BY.dta"
  outputname        <- "log_05_merge_enet_afid" 
  syntaxname        <- "05a_execute_merge_enet_afid" 
  
  .libPaths("R:\\Projekt4193\\Originaldaten\\RPakete_v4")
}

#==============================================================================
# 3. Call different R codes
#==============================================================================

# start the log file
#---------------------
sink(paste(outputpfad, "/", outputname, ".log", sep = ""), append = FALSE, type = c("output", "message"), split = TRUE)


# execute R codes
#-----------------
source(paste(syntaxpfad, "/", syntaxname, ".R", sep = ""), echo = TRUE, max.deparse.length = 99999)


# close the log file
#-------------------
sink()

